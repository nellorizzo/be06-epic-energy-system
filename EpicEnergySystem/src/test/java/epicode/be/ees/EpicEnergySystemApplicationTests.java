package epicode.be.ees;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import epicode.be.ees.dto.CityDto;
import epicode.be.ees.dto.ContactDto;
import epicode.be.ees.dto.CustomerDto;
import epicode.be.ees.dto.InvoiceDto;
import epicode.be.ees.dto.PostalAddressDto;
import epicode.be.ees.services.CustomerService;
import epicode.be.ees.services.InvoiceService;

@SpringBootTest
class EpicEnergySystemApplicationTests {

	@Autowired
	CustomerService customers;

	@Autowired
	InvoiceService invoices;

	@Test
	@DisplayName("Salva contatto e fattura")
	void saveContactAndInvoice() {
		var cust = assertDoesNotThrow(() -> //
		customers.save(CustomerDto.builder() //
				.withBusinessAddress( //
						PostalAddressDto.builder() //
								.withCity( //
										CityDto.builder() //
												.withName("Ascea") //
												.withProvinceAcronym("SA") //
												.build()) //
								.withCivic("17") //
								.withStreet("via XXIV Maggio") //
								.withZip("84046") //
								.build()) //
				.withContact(ContactDto.builder().withEmail("nello.rizzo@libero.it") //
						.withName("Nello") //
						.withSurname("Rizzo") //
						// .withPhone("00000000") //
						.build()) //
				.withEmail("nello@ennerre.info") //
				.withName("EnnErrE Consulting di Aniello Rizzo") //
				.withPec("nellorizzo@live.it") //
				.withPhone("3403571925") //
				.withType("DI") //
				.withVat("01234567890") //
				.build()) //
		);
		var invoice = assertDoesNotThrow(() -> //
		invoices.save(cust.getId(), InvoiceDto.builder().withAmount(BigDecimal.valueOf(12345))
				.withPublishDate(new Date()).withStatus("Non Pagata").build()));
		assertDoesNotThrow(() -> //
		invoices.save(cust.getId(), InvoiceDto.builder().withAmount(BigDecimal.valueOf(12345))
				.withPublishDate(new Date()).withStatus("Non Pagata").build()));
		assertDoesNotThrow(() -> //
		invoices.save(cust.getId(), InvoiceDto.builder().withAmount(BigDecimal.valueOf(12345))
				.withPublishDate(new Date()).withStatus("Non Pagata").build()));
		assertDoesNotThrow(() -> //
		invoices.save(cust.getId(), InvoiceDto.builder().withAmount(BigDecimal.valueOf(12345))
				.withPublishDate(new Date()).withStatus("Non Pagata").build()));
		assertNotNull(invoice);
	}

}
