package epicode.be.ees.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * Proprietà lette da application.properties.
 * 
 * @author Nello
 *
 */
@Component
@ConfigurationProperties(prefix = ApplicationProperties.CONFIGURATION_PROPERTY_PREFIX)
@Data
public class ApplicationProperties {
	/**
	 * Prefisso all'interno di application.properties.
	 */
	protected static final String CONFIGURATION_PROPERTY_PREFIX = "epicode.configuration";
	/**
	 * Configurazione della sorgente per le città e province.
	 */
	private CitiesConfiguration cities;
	/**
	 * Configurazione dei clienti.
	 */
	private CustomersConfiguration customers;
	/**
	 * Configurazione delle fatture.
	 */
	private InvoicesConfiguration invoices;
	/**
	 * Indica se eseguire il TestRunner.
	 */
	private boolean useRunner;

	/**
	 * Configurazione delle città.
	 * 
	 * @author Nello
	 *
	 */
	@Data
	public static class CitiesConfiguration {
		private String fileUri;
	}

	/**
	 * Configurazione dei clienti.
	 * 
	 * @author Nello
	 *
	 */
	@Data
	public static class CustomersConfiguration {
		private String[] defaultTypes;
		private int fakeCustomers;
	}

	/**
	 * Configurazione delle fatture.
	 * 
	 * @author Nello
	 *
	 */
	@Data
	public static class InvoicesConfiguration {
		private String[] defaultStatus;
		private int fakeInvoices;
	}
}
