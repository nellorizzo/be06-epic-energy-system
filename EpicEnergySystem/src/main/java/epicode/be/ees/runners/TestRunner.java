package epicode.be.ees.runners;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import epicode.be.ees.configuration.ApplicationProperties;
import epicode.be.ees.repositories.InvoiceRepository;
import epicode.be.ees.services.GeoService;
import epicode.be.ees.services.InvoiceService;

/**
 * Solo per test iniziali.
 * 
 * @author Nello
 *
 */
@Component
public class TestRunner implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(TestRunner.class);

	@Autowired
	GeoService geoService;

	@Autowired
	InvoiceService invoices;

	@Autowired
	ApplicationProperties properties;

	@Autowired
	InvoiceRepository invoiceRepo;

	@Override
	public void run(String... args) throws Exception {
		if (!properties.isUseRunner()) {
			log.info("Nothing to run...");
			return;
		}

		log.info("Start TestRunner");

		log.info("Città in provincia di Napoli:");
		geoService.getCities("NA", Pageable.unpaged()).getSlice().forEach(c -> log.info("-> {}", c));

		log.info("Test query max number: {}", invoices.getNextNumber(2022));

		log.info("Fatturato del customer con id = 1 nel 2022: {}",
				invoiceRepo.getAmountByCustomerAndYear(1, 2022).orElse(BigDecimal.ZERO));

		var m = invoiceRepo.getAmountByYear(2022);
		log.info("Records per fatturati 2022: {}", m.size());
		m.stream().forEach(i -> log.info("Customer = {} - Amount = {}", i.getCustomer().getName(), i.getAmount()));

		invoiceRepo.getPagedAmountByYear(2022, Pageable.unpaged()).getContent().stream()
				.forEach(c -> log.info("Item: {} {}", c.getCustomer().getName(), c.getAmount()));
		log.info("End TestRunner");
	}

}
