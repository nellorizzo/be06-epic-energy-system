package epicode.be.ees.runners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import epicode.be.ees.services.ConfigurationService;

/**
 * Configurazione iniziale.
 * 
 * @author Nello
 *
 */
@Component
public class ConfigurationRunner implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(ConfigurationRunner.class);

	@Autowired
	ConfigurationService service;

	@Override
	public void run(String... args) throws Exception {
		log.info("Start ConfigurationRunner");

		service.configure();

		log.info("End ConfigurationRunner");
	}

}
