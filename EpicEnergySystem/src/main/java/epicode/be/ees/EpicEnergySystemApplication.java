package epicode.be.ees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpicEnergySystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(EpicEnergySystemApplication.class, args);
	}

}
