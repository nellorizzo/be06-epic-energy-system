package epicode.be.ees.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base per tutti i controllers.
 * 
 * @author Nello
 *
 */
public class ControllerBase {
	/**
	 * Logger.
	 */
	protected final Logger log = LoggerFactory.getLogger(getClass());
}
