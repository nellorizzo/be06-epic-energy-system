package epicode.be.ees.controllers.rest.models.responses;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleResponseModel<T> extends ResponseModel {
	T content;

	@Builder(setterPrefix = "with")
	public SingleResponseModel(T content) {
		super(true);
		this.content = content;
	}
}
