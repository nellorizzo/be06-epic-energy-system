package epicode.be.ees.controllers.rest.models.responses;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ErrorResponseModel extends ResponseModel {
	private String reason;

	@Builder(setterPrefix = "with")
	public ErrorResponseModel(String reason) {
		super(false);
		this.reason = reason;
	}
}
