package epicode.be.ees.controllers.rest.models.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public abstract class ResponseModel {
	private boolean valid;
}
