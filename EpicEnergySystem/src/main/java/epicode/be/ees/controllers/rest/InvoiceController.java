package epicode.be.ees.controllers.rest;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import epicode.be.ees.controllers.ControllerBase;
import epicode.be.ees.controllers.rest.models.responses.ErrorResponseModel;
import epicode.be.ees.controllers.rest.models.responses.PagedResponseModel;
import epicode.be.ees.controllers.rest.models.responses.ResponseModel;
import epicode.be.ees.services.InvoiceService;

@RestController
@RequestMapping("/api/invoices")
public class InvoiceController extends ControllerBase {

	@Autowired
	InvoiceService service;

	@GetMapping("/revenues/{year}")
	public ResponseEntity<ResponseModel> getRevenues(@PathVariable int year,
			@RequestParam(required = false) BigDecimal min, @RequestParam(required = false) BigDecimal max,
			@PageableDefault(page = 0, size = 50, direction = Direction.DESC, sort = "amount") Pageable pageable) {
		try {
			if (min != null || max != null)
				return ResponseEntity.ok(new PagedResponseModel<>(service.getRevenues(year, min, max, pageable)));
			return ResponseEntity.ok(new PagedResponseModel<>(service.getRevenues(year, pageable)));
		} catch (Exception e) {
			log.error("Exception in InvoiceController.getRevenues [GET /api/invoices/revenues/{year}]", e);
			return ResponseEntity.badRequest().body(new ErrorResponseModel(e.getMessage()));
		}
	}

	@GetMapping(value = { "", "{customerId}", "{customerId}/{year}" })
	public ResponseEntity<ResponseModel> getInvoices(@PathVariable(required = false) Long customerId,
			@PathVariable(required = false) Integer year,
			@PageableDefault(page = 0, size = 50, direction = Direction.ASC, sort = "publishDate") Pageable pageable) {
		try {
			if (customerId == null)
				return ResponseEntity.ok(new PagedResponseModel<>(service.getAll(year, pageable)));
			return ResponseEntity.ok(new PagedResponseModel<>(service.getByCustomer(customerId, year, pageable)));
		} catch (Exception e) {
			log.error("Exception in InvoiceController.getInvoices [GET /api/invoices/{customerId}/{year}]", e);
			return ResponseEntity.badRequest().body(new ErrorResponseModel(e.getMessage()));
		}
	}

}
