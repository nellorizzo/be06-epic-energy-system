package epicode.be.ees.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import epicode.be.ees.controllers.ControllerBase;
import epicode.be.ees.controllers.rest.models.responses.ErrorResponseModel;
import epicode.be.ees.controllers.rest.models.responses.PagedResponseModel;
import epicode.be.ees.controllers.rest.models.responses.ResponseModel;
import epicode.be.ees.controllers.rest.models.responses.SingleResponseModel;
import epicode.be.ees.dto.CustomerDto;
import epicode.be.ees.services.CustomerService;
import epicode.be.ees.services.exceptions.EntityNotFoundException;

@RestController
@RequestMapping("/api/customers")
public class CustomerController extends ControllerBase {

	@Autowired
	CustomerService customers;

	@GetMapping
	public ResponseEntity<ResponseModel> list(@RequestParam(required = false) String search,
			@RequestParam(required = false) String type,
			@PageableDefault(page = 0, size = 50, sort = { "name" }) Pageable pageable) {
		try {
			return ResponseEntity.ok(new PagedResponseModel<CustomerDto>(customers.getAll(search, type, pageable)));
		} catch (Exception e) {
			log.error("Exception in CustomerController.list", e);
			return ResponseEntity.badRequest().body(new ErrorResponseModel(e.getMessage()));
		}
	}

	@GetMapping("{id}")
	public ResponseEntity<ResponseModel> get(@PathVariable long id) {
		try {
			var response = customers.get(id).orElseThrow(() -> new EntityNotFoundException(id, CustomerDto.class));
			return ResponseEntity.ok(new SingleResponseModel<CustomerDto>(response));
		} catch (Exception e) {
			log.error("Exception in CustomerController.get [GET /api/customers/{id}]", e);
			return ResponseEntity.badRequest().body(new ErrorResponseModel(e.getMessage()));
		}
	}

	@PostMapping
	public ResponseEntity<ResponseModel> add(@RequestBody CustomerDto customer) {
		try {
			var response = customers.save(customer);
			return ResponseEntity.created(UriComponentsBuilder.fromPath("/api/customers/{id}").build(response.getId()))
					.body(new SingleResponseModel<CustomerDto>(response));
		} catch (Exception e) {
			log.error("Exception in CustomerController.add [POST /api/customers]", e);
			return ResponseEntity.badRequest().body(new ErrorResponseModel(e.getMessage()));
		}
	}

	@DeleteMapping("{customerId}")
	public ResponseEntity<ResponseModel> delete(@PathVariable long customerId) {
		try {
			var response = customers.delete(customerId);
			return ResponseEntity.accepted().body(new SingleResponseModel<CustomerDto>(response));
		} catch (Exception e) {
			log.error("Exception in CustomerController.delete [DELETE /api/customers/{id}]", e);
			return ResponseEntity.badRequest().body(new ErrorResponseModel(e.getMessage()));
		}
	}
}
