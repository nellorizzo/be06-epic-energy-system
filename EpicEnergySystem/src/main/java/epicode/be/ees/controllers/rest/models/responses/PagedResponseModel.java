package epicode.be.ees.controllers.rest.models.responses;

import epicode.be.ees.dto.PagedResult;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PagedResponseModel<T> extends ResponseModel {
	private PagedResult<T> content;

	@Builder(setterPrefix = "with")
	public PagedResponseModel(PagedResult<T> content) {
		super(true);
		this.content = content;
	}
}
