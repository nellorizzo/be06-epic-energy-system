package epicode.be.ees.services;

/**
 * Servizio per la configurazione del sistema.
 * @author Nello
 *
 */
public interface ConfigurationService {
	/**
	 * Configura il sistema.
	 */
	void configure();
}
