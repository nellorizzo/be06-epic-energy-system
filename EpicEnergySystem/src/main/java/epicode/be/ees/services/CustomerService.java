package epicode.be.ees.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import epicode.be.ees.dto.CustomerDto;
import epicode.be.ees.dto.CustomerTypeDto;
import epicode.be.ees.dto.PagedResult;

/**
 * Servizio clienti.
 * 
 * @author Nello
 *
 */
public interface CustomerService extends BaseService {
	/**
	 * Salva un tipo di cliente.
	 * 
	 * @param type il tipo di cliente.
	 */
	CustomerTypeDto saveType(CustomerTypeDto type);

	/**
	 * Recupera l'elenco dei tipi di clienti.
	 */
	List<CustomerTypeDto> getTypes();

	/**
	 * Elimina un tipo di cliente.
	 * 
	 * @param id la chiave del cliente.
	 */
	CustomerTypeDto deleteType(long id);

	/**
	 * Recupera un tipo di cliente.
	 * 
	 * @param id la chiave per il recupero.
	 */
	Optional<CustomerTypeDto> getType(long id);

	/**
	 * Recupera i tipi che hanno un nome simile a quello passato
	 * 
	 * @param type il nome dal cercare.
	 */
	List<CustomerTypeDto> getType(String type);

	/**
	 * Salva un cliente.
	 * 
	 * @param cust dati del cliente.
	 */
	CustomerDto save(CustomerDto cust);

	/**
	 * Recupera i clienti.
	 * 
	 * @param searchFor parte del nome da cercare.
	 * @param type      tipologia di cliente.
	 */
	PagedResult<CustomerDto> getAll(String searchFor, String type, Pageable pageable);

	/**
	 * Recupera i clienti per ultimo contatto.
	 * 
	 * @param from data iniziale della ricerca.
	 * @param to   data finale della ricerca.
	 */
	PagedResult<CustomerDto> getAllByLastContact(Date from, Date to, Pageable pageable);

	/**
	 * Recupera i clienti per data di creazione.
	 * 
	 * @param from data iniziale della ricerca.
	 * @param to   data finale della ricerca.
	 */
	PagedResult<CustomerDto> getAllByCreation(Date from, Date to, Pageable pageable);

	/**
	 * Recupera un cliente tramite la chiave.
	 * 
	 * @param id la chiave.
	 */
	Optional<CustomerDto> get(long id);

	/**
	 * Elimina un cliente.
	 * 
	 * @param id la chiave.
	 */
	CustomerDto delete(long id);

}
