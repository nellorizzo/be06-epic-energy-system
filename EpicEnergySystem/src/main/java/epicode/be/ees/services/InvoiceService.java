package epicode.be.ees.services;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import epicode.be.ees.dto.InvoiceDto;
import epicode.be.ees.dto.InvoiceStatusDto;
import epicode.be.ees.dto.PagedResult;
import epicode.be.ees.dto.RevenueDto;

/**
 * Servizi per fatture.
 * 
 * @author Nello
 *
 */
public interface InvoiceService extends BaseService {
	/**
	 * Salva uno stato fattura.
	 * 
	 * @param status lo stato della fattura.
	 */
	InvoiceStatusDto saveStatus(InvoiceStatusDto status);

	/**
	 * Recupera l'elenco degli stati fattura.
	 */
	List<InvoiceStatusDto> getAllStatus();

	/**
	 * Elimina un tipo di stato fattura.
	 * 
	 * @param id la chiave.
	 */
	InvoiceStatusDto deleteStatus(long id);

	/**
	 * Recupera uno stato fattura.
	 * 
	 * @param id la chiave per il recupero.
	 */
	Optional<InvoiceStatusDto> getStatus(long id);

	/**
	 * Recupera gli stati che hanno un nome simile a quello passato
	 * 
	 * @param status il nome dal cercare.
	 */
	List<InvoiceStatusDto> getStatus(String status);

	/**
	 * Salva una fattura.
	 * 
	 * @param customerId id del cliente.
	 * @param dto        dati per il salvataggio.
	 */
	InvoiceDto save(Long customerId, InvoiceDto dto);

	/**
	 * Recupera una fattura.
	 * 
	 * @param number il numero.
	 * @param year   l'anno.
	 */
	InvoiceDto get(Integer number, Integer year);

	/**
	 * Calcola il prossimo numero per una fattura in un anno.
	 * 
	 * @param year l'anno di riferimento.
	 */
	Integer getNextNumber(Integer year);

	/**
	 * Recupera le fatture di un cliente.
	 * 
	 * @param customerId la chiave del cliente.
	 */
	PagedResult<InvoiceDto> getAll(Long customerId, Pageable pageable);

	/**
	 * Recupera i fatturati per anno.
	 * 
	 * @param year     l'anno di riferimento.
	 * @param pageable le informazioni per la paginazione.
	 */
	PagedResult<RevenueDto> getRevenues(int year, Pageable pageable);

	/**
	 * Recupera i fatturati per anno.
	 * 
	 * @param year     l'anno di riferimento.
	 * @param min      il fatturato minimo da considerare.
	 * @param max      il fatturato massimo da considerare.
	 * @param pageable le informazioni per la paginazione.
	 */
	PagedResult<RevenueDto> getRevenues(int year, BigDecimal min, BigDecimal max, Pageable pageable);

	/**
	 * Recupera le fatture per cliente.
	 * 
	 * @param customerId id del cliente.
	 * @param year       l'anno di riferimento (null se si vuole recuperarle tutte).
	 * @param pageable   le informazioni di paginazione.
	 * @return
	 */
	PagedResult<InvoiceDto> getByCustomer(long customerId, Integer year, Pageable pageable);

	PagedResult<InvoiceDto> getAll(Integer year, Pageable pageable);
}
