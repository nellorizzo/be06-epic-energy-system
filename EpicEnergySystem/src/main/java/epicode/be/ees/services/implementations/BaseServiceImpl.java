package epicode.be.ees.services.implementations;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;

import epicode.be.ees.dto.PagedResult;
import epicode.be.ees.dto.converters.DtoFromEntityConverter;
import epicode.be.ees.entities.BaseEntity;
import epicode.be.ees.services.BaseService;
import epicode.be.ees.services.exceptions.ServiceException;

/**
 * Implementazione delle funzionalità di base per tutti i servizi.
 * 
 * @author Nello
 *
 */
public class BaseServiceImpl implements BaseService {

	protected final Logger log = LoggerFactory.getLogger(getClass());

	private static final String SERVICE_EXCEPTION_MSG = "Service exception in %s";
	private static final String UNATTENDED_EXCEPTION_MSG = "Unattended exception in %s";

	/**
	 * Converte una risposta paginata di un servizio in una risposta paginata Dto.
	 * 
	 * @param <T>       Tipo di DTO
	 * @param <E>       Tipo di Entity
	 * @param page      le informazioni da convertire.
	 * @param converter il convertitore usato.
	 */
	public <T, E extends BaseEntity> PagedResult<T> convert(Page<E> page, DtoFromEntityConverter<T, E> converter) {
		return new PagedResult<>(convert(page.getContent(), converter), page.isFirst(), page.isLast(), page.getNumber(),
				page.getSize(), page.getTotalElements(), page.getTotalPages());
	}

	/**
	 * Converte una lista di un servizio in una lista Dto.
	 * 
	 * @param <T>       Tipo di DTO
	 * @param <E>       Tipo di Entity
	 * @param list      le informazioni da convertire
	 * @param converter il convertitore utilizzato
	 */
	public <T, E extends BaseEntity> List<T> convert(List<E> list, DtoFromEntityConverter<T, E> converter) {
		return list.stream().map(converter::convert).collect(Collectors.toList());
	}

	/**
	 * Registra e propaga un'eccezione di un servizio.
	 * 
	 * @param caller le informazioni sul punto in cui l'eccezione è avvenuta.
	 * @param e      l'eccezione che si è verificata.
	 * @return il metodo non restituisce nulla perché scatena l'eccezione (di tipo
	 *         {@code ServiceException}) passata come parametro.
	 */
	protected void handle(String caller, Exception e) {
		try {
			throw e;
		} catch (ServiceException ex) {
			log.error(String.format(SERVICE_EXCEPTION_MSG, caller), e);
			throw ex;
		} catch (Exception ex) {
			log.error(String.format(UNATTENDED_EXCEPTION_MSG, caller), e);
			throw new ServiceException(e);
		}
	}
}
