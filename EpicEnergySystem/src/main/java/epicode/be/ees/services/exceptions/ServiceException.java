package epicode.be.ees.services.exceptions;

/**
 * Base per le eccezioni dei servizi.
 * 
 * @author Nello
 *
 */
public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_MSG = "Service exception";
	private static final String DEFAULT_THROWABLE_MSG = "Service exception (see inner cause)";

	public ServiceException() {
		this(DEFAULT_MSG);
	}

	public ServiceException(String message) {
		this(message, null);
	}

	public ServiceException(Throwable cause) {
		this(DEFAULT_THROWABLE_MSG, cause);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
