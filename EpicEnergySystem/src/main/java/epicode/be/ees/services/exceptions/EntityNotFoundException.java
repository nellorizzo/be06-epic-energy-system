package epicode.be.ees.services.exceptions;

import lombok.Getter;

/**
 * Comunica l'impossibilità di recuperare un'entità tramite una chiave.
 * 
 * @author Nello
 *
 */
@Getter
public class EntityNotFoundException extends ServiceException {

	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_MSG = "Unable to find entity of type %s by key = %s";
	private static final String DEFAULT_THROWABLE_MSG = "Unable to find entity of type %s by key = %s (see inner cause)";

	/**
	 * La chiave di ricerca.
	 */
	private final String key;
	/**
	 * Il tipo di entità cercata.
	 */
	private final Class<?> clazz;

	public EntityNotFoundException(String key, Class<?> clazz) {
		super(String.format(DEFAULT_MSG, clazz.getName(), key), null);
		this.key = key;
		this.clazz = clazz;
	}

	public EntityNotFoundException(String key, Class<?> clazz, Throwable cause) {
		super(String.format(DEFAULT_THROWABLE_MSG, clazz.getName(), key), cause);
		this.key = key;
		this.clazz = clazz;
	}

	public EntityNotFoundException(Long id, Class<?> clazz) {
		this(id.toString(), clazz);
	}

	public EntityNotFoundException(Long id, Class<?> clazz, Throwable cause) {
		this(id.toString(), clazz, cause);
	}
}
