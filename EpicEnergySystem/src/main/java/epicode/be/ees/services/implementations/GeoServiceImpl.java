package epicode.be.ees.services.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import epicode.be.ees.dto.CityDto;
import epicode.be.ees.dto.PagedResult;
import epicode.be.ees.dto.ProvinceDto;
import epicode.be.ees.dto.converters.CityFromEntityConverter;
import epicode.be.ees.dto.converters.ProvinceFromEntityConverter;
import epicode.be.ees.entities.addresses.City;
import epicode.be.ees.entities.addresses.Province;
import epicode.be.ees.repositories.CityRepository;
import epicode.be.ees.repositories.ProvinceRepository;
import epicode.be.ees.services.GeoService;
import epicode.be.ees.services.exceptions.EntityNotFoundException;
import java.util.Collections;

@Service
public class GeoServiceImpl extends BaseServiceImpl implements GeoService {

	@Autowired
	CityRepository cities;

	@Autowired
	ProvinceRepository provinces;

	@Autowired
	CityFromEntityConverter cityConv;

	@Autowired
	ProvinceFromEntityConverter provConv;

	@Override
	public CityDto saveCity(CityDto city) {
		try {
			var p = provinces.findByAcronym(city.getProvinceAcronym()).orElse(null);
			if (p == null)
				p = provinces.save(Province.builder().withAcronym(city.getProvinceAcronym())
						.withName(String.format("Must Change (Acronym = %s)", city.getProvinceAcronym())).build());
			var c = City.builder().withCapital(city.isCapital()).withFiscalCode(city.getFiscalCode())
					.withName(city.getName()).withProvince(p).withId(city.getId()).build();
			return cityConv.convert(cities.save(c));
		} catch (Exception e) {
			handle("CityService.saveCity(CityDto)", e);
		}
		return null;
	}

	@Override
	public Optional<CityDto> getCity(long id) {
		try {
			var city = cities.findById(id);
			if (city.isEmpty())
				return Optional.empty();
			return Optional.of(cityConv.convert(city.get()));
		} catch (Exception e) {
			handle("CityService.getCity(long)", e);
		}
		return Optional.empty();
	}

	@Override
	public PagedResult<CityDto> getCities(String province, Pageable pageable) {
		try {
			if (province == null)
				return convert(cities.findAll(pageable), cityConv);
			return convert(cities.findAllByProvinceAcronym(province, pageable), cityConv);
		} catch (Exception e) {
			handle("CityService.getCities(String, Pageable)", e);
		}
		return null;
	}

	@Override
	public CityDto deleteCity(long id) {
		try {
			var city = getCity(id).orElseThrow(() -> new EntityNotFoundException(id, CityDto.class));
			cities.deleteById(id);
			return city;
		} catch (Exception e) {
			handle("CityService.deleteCity(long)", e);
		}
		return null;
	}

	@Override
	public ProvinceDto saveProvince(ProvinceDto province) {
		try {
			var p = Province.builder().withAcronym(province.getAcronym()).withName(province.getName())
					.withId(province.getId()).build();
			return provConv.convert(provinces.save(p));
		} catch (Exception e) {
			handle("CityService.saveProvince(ProvinceDto)", e);
		}
		return null;
	}

	@Override
	public Optional<ProvinceDto> getProvince(String provinceAcronym) {
		try {
			var province = provinces.findByAcronym(provinceAcronym);
			if (province.isEmpty())
				return Optional.empty();
			return Optional.of(provConv.convert(province.get()));
		} catch (Exception e) {
			handle("CityService.getProvince(String)", e);
		}
		return Optional.empty();
	}

	@Override
	public List<ProvinceDto> getProvinces() {
		try {
			return convert(provinces.findAll(Sort.by("acronym")), provConv);
		} catch (Exception e) {
			handle("CityService.getProvinces()", e);
		}
		return Collections.emptyList();
	}

	@Override
	public ProvinceDto deleteProvince(String acronym) {
		try {
			var p = getProvince(acronym).orElseThrow(() -> new EntityNotFoundException(acronym, Province.class));
			provinces.deleteById(p.getId());
			return p;
		} catch (Exception e) {
			handle("CityService.deleteProvince(String)", e);
		}
		return null;
	}

	@Override
	public Optional<CityDto> getCity(String name, String provinceAcronym) {
		try {
			var city = cities.findByNameAndProvinceAcronym(name, provinceAcronym);
			if (city.isEmpty())
				return Optional.empty();
			return Optional.of(cityConv.convert(city.get()));
		} catch (Exception e) {
			handle("CityService.getCity(String, String)", e);
		}
		return Optional.empty();
	}

}
