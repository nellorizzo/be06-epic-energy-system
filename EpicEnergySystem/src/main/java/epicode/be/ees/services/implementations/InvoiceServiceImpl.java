package epicode.be.ees.services.implementations;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import epicode.be.ees.dto.CustomerDto;
import epicode.be.ees.dto.InvoiceDto;
import epicode.be.ees.dto.InvoiceStatusDto;
import epicode.be.ees.dto.PagedResult;
import epicode.be.ees.dto.RevenueDto;
import epicode.be.ees.dto.converters.InvoiceFromEntityConverter;
import epicode.be.ees.dto.converters.InvoiceStatusFromEntityConverter;
import epicode.be.ees.dto.converters.implementations.EntitiesFromDtoConverters;
import epicode.be.ees.entities.Invoice;
import epicode.be.ees.entities.InvoiceStatus;
import epicode.be.ees.repositories.InvoiceRepository;
import epicode.be.ees.repositories.InvoiceStatusRepository;
import epicode.be.ees.repositories.InvoiceRepository.AmountByYear;
import epicode.be.ees.services.CustomerService;
import epicode.be.ees.services.InvoiceService;
import epicode.be.ees.services.exceptions.EntityNotFoundException;

@Service
public class InvoiceServiceImpl extends BaseServiceImpl implements InvoiceService {

	@Autowired
	InvoiceRepository invoices;
	@Autowired
	InvoiceStatusRepository statusRepo;

	@Autowired
	InvoiceFromEntityConverter invoiceConverter;

	@Autowired
	InvoiceStatusFromEntityConverter statusConverter;

	@Autowired
	CustomerService customers;

	@Autowired
	EntitiesFromDtoConverters toEntity;

	@Override
	public InvoiceStatusDto saveStatus(InvoiceStatusDto status) {
		try {
			return statusConverter
					.convert(statusRepo.save(InvoiceStatus.builder().withDescription(status.getDescription())
							.withId(status.getId()).withName(status.getName()).build()));
		} catch (Exception e) {
			handle("InvoiceService.saveStatus(InvoiceStatusDto)", e);
		}
		return null;
	}

	@Override
	public List<InvoiceStatusDto> getAllStatus() {
		try {
			return convert(statusRepo.findAll(), statusConverter);
		} catch (Exception e) {
			handle("InvoiceService.getAllStatus()", e);
		}
		return Collections.emptyList();
	}

	@Override
	public InvoiceStatusDto deleteStatus(long id) {
		try {
			var status = statusRepo.findById(id)
					.orElseThrow(() -> new EntityNotFoundException(id, InvoiceStatusDto.class));
			statusRepo.delete(status);
			return statusConverter.convert(status);
		} catch (Exception e) {
			handle("InvoiceService.deleteStatus(long)", e);
		}
		return null;
	}

	@Override
	public Optional<InvoiceStatusDto> getStatus(long id) {
		try {
			var status = statusRepo.findById(id);
			if (!status.isEmpty())
				return Optional.of(statusConverter.convert(status.get()));
		} catch (Exception e) {
			handle("InvoiceService.getStatus(long)", e);
		}
		return Optional.empty();
	}

	@Override
	public List<InvoiceStatusDto> getStatus(String status) {
		try {
			if (status == null)
				return convert(statusRepo.findAll(), statusConverter);
			return convert(statusRepo.findAllByNameContains(status), statusConverter);
		} catch (Exception e) {
			handle("InvoiceService.getStatus(String)", e);
		}
		return Collections.emptyList();
	}

	// *********************************
	// Convertitori da Dto verso Entity

	private InvoiceStatus fromDto(String status) {
		return statusRepo.findByName(status)
				.orElseThrow(() -> new EntityNotFoundException(status, InvoiceStatusDto.class));
	}

	// Fine convertitori da Dto a Entity
	// ************************************************

	@Override
	@Transactional
	public InvoiceDto save(Long customerId, InvoiceDto dto) {
		try {
			var year = dto.getPublishDate().toInstant().atZone(ZoneId.systemDefault()).getYear();
			var number = dto.getId() == null || dto.getId() == 0 ? getNextNumber(year) : dto.getNumber();
			var invoice = Invoice.builder().withAmount(dto.getAmount()).withId(dto.getId()).withNumber(number)
					.withPublishDate(dto.getPublishDate()).withStatus(fromDto(dto.getStatus())).withYear(year)
					.withCustomer(toEntity.fromDto(customers.get(customerId)
							.orElseThrow(() -> new EntityNotFoundException(customerId, CustomerDto.class))))
					.build();
			var result = invoiceConverter.convert(invoices.save(invoice));
			customers.save(customers.get(customerId).orElseThrow());
			return result;
		} catch (Exception e) {
			handle("InvoiceService.save(InvoiceDto)", e);
		}
		return null;
	}

	@Override
	public InvoiceDto get(Integer number, Integer year) {
		try {
			var invoice = invoices.findByNumberAndYear(number, year)
					.orElseThrow(() -> new EntityNotFoundException(String.format("{numer=%d, year=%d}", number, year),
							InvoiceDto.class));
			return invoiceConverter.convert(invoice);
		} catch (Exception e) {
			handle("InvoiceService.get(Integer, Integer)", e);
		}
		return null;
	}

	@Override
	public Integer getNextNumber(Integer year) {
		return invoices.findNextNumberOfYear(year).orElse(1);
	}

	@Override
	public PagedResult<InvoiceDto> getAll(Long customerId, Pageable pageable) {
		try {
			return convert(invoices.findAllByCustomerId(customerId, pageable), invoiceConverter);
		} catch (Exception e) {
			handle("InvoceService.getAll(Long, Pageable)", e);
		}
		return null;
	}

	public List<RevenueDto> convertRevenues(List<AmountByYear> list) {
		return list.stream()
				.map(i -> RevenueDto.builder().withCustomerId(i.getCustomer().getId())
						.withCustomerName(i.getCustomer().getName()).withCustomerVat(i.getCustomer().getVat())
						.withRevenues(i.getAmount()).build())
				.collect(Collectors.toList());
	}

	public PagedResult<RevenueDto> convertRevenues(Page<AmountByYear> page) {
		var list = convertRevenues(page.getContent());
		return new PagedResult<>(list, page.isFirst(), page.isLast(), page.getSize(), page.getNumber(),
				page.getTotalElements(), page.getTotalPages());
	}

	@Override
	public PagedResult<RevenueDto> getRevenues(int year, Pageable pageable) {
		try {
			return convertRevenues(invoices.getPagedAmountByYear(year, pageable));
		} catch (Exception e) {
			handle("InvoiceService.getRevenues(year, Pageable)", e);
		}
		return null;
	}

	@Override
	public PagedResult<RevenueDto> getRevenues(int year, BigDecimal min, BigDecimal max, Pageable pageable) {
		try {
			if (min == null && max == null)
				return getRevenues(year, pageable);
			if (min == null)
				min = BigDecimal.ZERO;
			if (max == null)
				max = BigDecimal.valueOf(Long.MAX_VALUE);
			return convertRevenues(invoices.getPagedAmountByYear(year, min, max, pageable));
		} catch (Exception e) {
			handle("InvoiceService.getRevenues(year, BigDecimal, BigDecimal, Pageable)", e);
		}
		return null;
	}

	@Override
	public PagedResult<InvoiceDto> getByCustomer(long customerId, Integer year, Pageable pageable) {
		try {
			if (year == null)
				return convert(invoices.getByCustomerId(customerId, pageable), invoiceConverter);
			return convert(invoices.findAllByCustomerIdAndYear(customerId, year, pageable), invoiceConverter);
		} catch (Exception e) {
			handle("InvoiceService.getRevenues(long, Integer, Pageable)", e);
		}
		return null;
	}

	@Override
	public PagedResult<InvoiceDto> getAll(Integer year, Pageable pageable) {
		try {
			if (year == null)
				return convert(invoices.findAll(pageable), invoiceConverter);
			return convert(invoices.findAllByYear(year, pageable), invoiceConverter);
		} catch (Exception e) {
			handle("InvoiceService.getRevenues(year, Pageable)", e);
		}
		return null;
	}

}
