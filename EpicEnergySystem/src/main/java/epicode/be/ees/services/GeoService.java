package epicode.be.ees.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import epicode.be.ees.dto.CityDto;
import epicode.be.ees.dto.PagedResult;
import epicode.be.ees.dto.ProvinceDto;

/**
 * Servizio di gestione dei dati geografici.
 * 
 * @author Nello
 *
 */
public interface GeoService {
	/**
	 * Salva una città.
	 * 
	 * @param city dati da salvare.
	 */
	CityDto saveCity(CityDto city);

	/**
	 * Recupera tramite chiave.
	 * 
	 * @param id la chiave.
	 */
	Optional<CityDto> getCity(long id);

	/**
	 * Recupera tramite provincia.
	 * 
	 * @param province la provincia.
	 */
	PagedResult<CityDto> getCities(String province, Pageable pageable);

	/**
	 * Elimina una città.
	 * 
	 * @param id la chiave.
	 */
	CityDto deleteCity(long id);

	/**
	 * Salva una provincia.
	 * 
	 * @param province i dati da salvare.
	 */
	ProvinceDto saveProvince(ProvinceDto province);

	/**
	 * Recupera una provincia tramite la sigla.
	 * 
	 * @param provinceAcronym la sigla.
	 */
	Optional<ProvinceDto> getProvince(String provinceAcronym);

	/**
	 * Elenco delle province.
	 */
	List<ProvinceDto> getProvinces();

	/**
	 * Elimina una provincia.
	 * 
	 * @param acronym la sigla.
	 */
	ProvinceDto deleteProvince(String acronym);

	/**
	 * Recupera una città per nome e sigla della provincia.
	 * 
	 * @param name            nome della città.
	 * @param provinceAcronym sigla della provincia.
	 * @return
	 */
	Optional<CityDto> getCity(String name, String provinceAcronym);
}
