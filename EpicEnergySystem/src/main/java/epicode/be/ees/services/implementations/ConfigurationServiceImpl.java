package epicode.be.ees.services.implementations;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.github.javafaker.Faker;

import epicode.be.ees.configuration.ApplicationProperties;
import epicode.be.ees.dto.CityDto;
import epicode.be.ees.dto.ContactDto;
import epicode.be.ees.dto.CustomerDto;
import epicode.be.ees.dto.CustomerTypeDto;
import epicode.be.ees.dto.InvoiceDto;
import epicode.be.ees.dto.InvoiceStatusDto;
import epicode.be.ees.dto.PostalAddressDto;
import epicode.be.ees.dto.ProvinceDto;
import epicode.be.ees.services.ConfigurationService;
import epicode.be.ees.services.CustomerService;
import epicode.be.ees.services.GeoService;
import epicode.be.ees.services.InvoiceService;
import utils.cities.loader.CitiesLoader;

@Service
public class ConfigurationServiceImpl extends BaseServiceImpl implements ConfigurationService {

	@Autowired
	GeoService geoService;

	@Autowired
	CustomerService customerService;

	@Autowired
	InvoiceService invoiceService;

	@Autowired
	ApplicationProperties properties;

	private final Faker faker = new Faker();
	private final Random rnd = new Random();

	@Override
	public void configure() {
		if (geoService.getProvinces().isEmpty())
			loadCities();
		if (customerService.getTypes().isEmpty()) {
			addCustomersType();
			populateFakeCustomers();
		}
		if (invoiceService.getAllStatus().isEmpty()) {
			loadInvoiceStatus();
			populateFakeInvoices();
		}
	}

	/**
	 * Popola il database con fatture fake.
	 */
	static final long THREE_YEAR_AGO = 3 * 31557600000l;

	private void populateFakeInvoices() {
		log.info("Populating invoices... please wait");
		var total = properties.getInvoices().getFakeInvoices();
		var status = properties.getInvoices().getDefaultStatus();
		List<Long> customers = customerService.getAll(null, null, Pageable.unpaged()).getSlice().stream().map(c -> c.getId())
				.collect(Collectors.toList());

		var now = new Date();

		while (total-- > 0) {
			var inv = InvoiceDto.builder().withAmount(BigDecimal.valueOf(rnd.nextInt(Integer.MAX_VALUE)))
					.withStatus(status[rnd.nextInt(status.length)])
					.withPublishDate(new Date(now.getTime() - rnd.nextLong() % THREE_YEAR_AGO)).build();
			invoiceService.save(customers.get(rnd.nextInt(customers.size())), inv);
		}
		log.info("Invoices created");
	}

	/**
	 * Popola il database dei clienti con entità fittizie.
	 */
	private void populateFakeCustomers() {
		log.info("Populating customers... please wait");
		var total = properties.getCustomers().getFakeCustomers();
		var types = properties.getCustomers().getDefaultTypes();
		while (total-- > 0) {
			var cust = CustomerDto.builder().withBusinessAddress(fakeAddress()).withContact(fakeContact())
					.withEmail(faker.internet().emailAddress()).withName(faker.company().name())
					.withPec(faker.internet().emailAddress()).withPhone(faker.phoneNumber().cellPhone())
					.withType(types[rnd.nextInt(types.length)]).withVat(fakeVat())
					.withWorkAddress(rnd.nextFloat() > .5 ? null : fakeAddress()).build();
			try {
				customerService.save(cust);
			} catch (Exception e) {
				log.error(String.format("Failed to create user %s", cust), e);
			}
		}
		log.info("Customers created on database");
	}

	private String fakeVat() {
		StringBuilder sb = new StringBuilder();
		for (var c = 0; c < 11; ++c)
			sb.append((char) ('0' + rnd.nextInt(10)));
		return sb.toString();
	}

	private ContactDto fakeContact() {
		return ContactDto.builder().withEmail(rnd.nextFloat() > .5 ? null : faker.internet().emailAddress())
				.withName(faker.name().firstName())
				.withPhone(rnd.nextFloat() > .5 ? null : faker.phoneNumber().cellPhone())
				.withSurname(faker.name().lastName()).build();
	}

	private PostalAddressDto fakeAddress() {
		var zip = faker.address().zipCode();
		var state = faker.address().state();
		var acronym = state.substring(0, 2).toUpperCase();
		var loc = faker.address().cityName();
		var province = geoService.getProvince(acronym);
		if (province.isEmpty())
			province = Optional
					.of(geoService.saveProvince(ProvinceDto.builder().withName(state).withAcronym(acronym).build()));
		var city = geoService.getCity(loc, acronym).orElse(geoService
				.saveCity(CityDto.builder().withName(loc).withProvinceAcronym(province.get().getAcronym()).build()));
		return PostalAddressDto.builder().withCity(city)
				.withCivic(rnd.nextFloat() > .5 ? null : faker.address().buildingNumber())
				.withLocation(rnd.nextBoolean() ? null : faker.address().cityPrefix())
				.withStreet(faker.address().streetAddress()).withZip(zip.substring(0, 5)).build();
	}

	/**
	 * Carica gli stati fattura di default.
	 */
	private void loadInvoiceStatus() {
		try {
			log.info("Populating invoice status... please wait");
			for (var t : properties.getInvoices().getDefaultStatus()) {
				invoiceService.saveStatus(InvoiceStatusDto.builder().withName(t).build());
			}
			log.info("Added {} invoice status to database", properties.getInvoices().getDefaultStatus().length);
		} catch (Exception e) {
			handle("ConfigurationService.loadCustomersType()", e);
		}
	}

	/**
	 * Carica le tipologie di cliente di default.
	 */
	private void addCustomersType() {
		try {
			log.info("Populating customer types... please wait");
			for (var t : properties.getCustomers().getDefaultTypes())
				customerService.saveType(CustomerTypeDto.builder().withType(t).build());
			log.info("Added {} to database", properties.getCustomers().getDefaultTypes().length);
		} catch (Exception e) {
			handle("ConfigurationService.loadCustomersType()", e);
		}

	}

	/**
	 * Carica città e province.
	 */
	private void loadCities() {
		try {
			var f = new ClassPathResource(properties.getCities().getFileUri());
			log.info("Loading cities from text file {}... please wait", f.getFilename());
			var c = CitiesLoader.load(f.getInputStream(), "ISO-8859-1");
			c.stream().map(d -> d.getProvince()).distinct().forEach(d -> geoService
					.saveProvince(ProvinceDto.builder().withAcronym(d.getAcronym()).withName(d.getName()).build()));
			c.stream().forEach(d -> {
				geoService.saveCity(CityDto.builder().withCapital(d.isCapital()).withFiscalCode(d.getFiscalCode())
						.withName(d.getName()).withProvinceAcronym(d.getProvince().getAcronym()).build());
			});
			log.info("Loaded {} cities", c.size());
		} catch (Exception e) {
			handle("ConfigurationService.loadCities()", e);
		}
	}
}
