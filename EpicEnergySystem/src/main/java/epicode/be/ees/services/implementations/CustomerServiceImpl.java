package epicode.be.ees.services.implementations;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import epicode.be.ees.dto.CustomerDto;
import epicode.be.ees.dto.CustomerTypeDto;
import epicode.be.ees.dto.PagedResult;
import epicode.be.ees.dto.converters.CustomerFromEntityConverter;
import epicode.be.ees.dto.converters.CustomerTypeFromEntityConverter;
import epicode.be.ees.dto.converters.implementations.EntitiesFromDtoConverters;
import epicode.be.ees.entities.CustomerType;
import epicode.be.ees.repositories.CustomerRepository;
import epicode.be.ees.repositories.CustomerTypeRepository;
import epicode.be.ees.services.CustomerService;
import epicode.be.ees.services.GeoService;
import epicode.be.ees.services.exceptions.EntityNotFoundException;

import java.util.Collections;
import java.util.Date;

@Service
public class CustomerServiceImpl extends BaseServiceImpl implements CustomerService {

	@Autowired
	CustomerTypeRepository types;

	@Autowired
	CustomerTypeFromEntityConverter typeConverter;

	@Autowired
	CustomerRepository customers;

	@Autowired
	CustomerFromEntityConverter custConv;

	@Autowired
	EntitiesFromDtoConverters toEntity;

	@Autowired
	GeoService geo;

	@Override
	public CustomerTypeDto saveType(CustomerTypeDto type) {
		try {
			return typeConverter.convert(types.save(CustomerType.builder().withDescription(type.getDescription())
					.withId(type.getId()).withType(type.getType()).build()));
		} catch (Exception e) {
			handle("CustomerService.saveType(CustomerTypeDto)", e);
		}
		return null;
	}

	@Override
	public List<CustomerTypeDto> getTypes() {
		try {
			return convert(types.findAll(), typeConverter);
		} catch (Exception e) {
			handle("CustomerService.getTypes()", e);
		}
		return Collections.emptyList();
	}

	@Override
	public CustomerTypeDto deleteType(long id) {
		try {
			var type = types.findById(id).orElseThrow(() -> new EntityNotFoundException(id, CustomerTypeDto.class));
			types.deleteById(id);
			return typeConverter.convert(type);
		} catch (Exception e) {
			handle("CustomerService.deleteType(long)", e);
		}
		return null;
	}

	@Override
	public Optional<CustomerTypeDto> getType(long id) {
		try {
			var type = types.findById(id);
			if (!type.isEmpty())
				return Optional.of(typeConverter.convert(type.get()));
		} catch (Exception e) {
			handle("CustomerService.getType(long)", e);
		}
		return Optional.empty();
	}

	@Override
	public List<CustomerTypeDto> getType(String type) {
		try {
			var list = types.findAllByTypeContains(type);
			return convert(list, typeConverter);
		} catch (Exception e) {
			handle("CustomerService.getType(String)", e);
		}
		return Collections.emptyList();
	}

	@Override
	@Transactional
	public CustomerDto save(CustomerDto cust) {
		try {
			cust.setLastContact(new Date());
			var bac = cust.getBusinessAddress().getCity();
			if (geo.getCity(bac.getName(), bac.getProvinceAcronym()).isEmpty()) {
				geo.saveCity(bac);
			}
			if (cust.getWorkAddress() != null) {
				var wac = cust.getWorkAddress().getCity();
				if (geo.getCity(wac.getName(), wac.getProvinceAcronym()).isEmpty()) {
					geo.saveCity(wac);
				}
			}
			var type = types.findByType(cust.getType()).orElse(null);
			if (type == null)
				types.save(CustomerType.builder().withType(cust.getType()).build());
			var c = toEntity.fromDto(cust);
			return custConv.convert(customers.save(c));
		} catch (Exception e) {
			handle("CustomerService.save(CustomerDto)", e);
		}
		return null;
	}

	@Override
	public PagedResult<CustomerDto> getAll(String searchFor, String type, Pageable pageable) {
		try {
			if (searchFor == null) {
				if (type == null)
					return convert(customers.findAll(pageable), custConv);
				else
					return convert(customers.findAllByTypeTypeIgnoreCase(type, pageable), custConv);
			} else if (type == null)
				return convert(customers.findAllByNameContainsIgnoreCase(searchFor, pageable), custConv);
			return convert(
					customers.findAllByNameContainsIgnoreCaseAndTypeTypeIgnoreCase(searchFor, type, pageable),
					custConv);
		} catch (Exception e) {
			handle("CustomerService.getAll(String, Pageable)", e);
		}
		return null;
	}

	@Override
	public PagedResult<CustomerDto> getAllByLastContact(Date from, Date to, Pageable pageable) {
		try {
			return convert(customers.findAllByLastContactBetween(from, to, pageable), custConv);
		} catch (Exception e) {
			handle("CustomerService.getAllByLastContact(Date, Date, Pageable)", e);
		}
		return null;
	}

	@Override
	public PagedResult<CustomerDto> getAllByCreation(Date from, Date to, Pageable pageable) {
		try {
			return convert(customers.findAllByCreatedAtBetween(from, to, pageable), custConv);
		} catch (Exception e) {
			handle("CustomerService.getAllByCreation(Date, Date, Pageable)", e);
		}
		return null;
	}

	@Override
	public Optional<CustomerDto> get(long id) {
		try {
			var cust = customers.findById(id);
			if (cust.isPresent())
				return Optional.of(custConv.convert(cust.get()));
		} catch (Exception e) {
			handle("CustomerService.get(Long)", e);
		}
		return Optional.empty();
	}

	@Override
	public CustomerDto delete(long id) {
		try {
			var cust = customers.findById(id).orElseThrow(() -> new EntityNotFoundException(id, CustomerDto.class));
			customers.delete(cust);
			return custConv.convert(cust);
		} catch (Exception e) {
			handle("CustomerService.delete(Long)", e);
		}
		return null;
	}

}
