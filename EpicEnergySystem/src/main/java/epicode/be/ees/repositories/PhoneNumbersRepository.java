package epicode.be.ees.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import epicode.be.ees.entities.addresses.PhoneAddress;

/**
 * Repository per numeri di telefono.
 * 
 * @author Nello
 *
 */
public interface PhoneNumbersRepository extends JpaRepository<PhoneAddress, Long> {
	/**
	 * Ricerca per numero.
	 * 
	 * @param text il numero di telefono.
	 */
	Optional<PhoneAddress> findByNumber(String text);

}
