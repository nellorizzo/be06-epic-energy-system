package epicode.be.ees.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import epicode.be.ees.entities.addresses.Province;

/**
 * Repository per province.
 * 
 * @author Nello
 *
 */
@Repository
public interface ProvinceRepository extends JpaRepository<Province, Long> {
	/**
	 * Cerca una provincia tramite la sigla.
	 * 
	 * @param acronym la sigla.
	 */
	Optional<Province> findByAcronym(String acronym);

}
