package epicode.be.ees.repositories;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import epicode.be.ees.entities.Customer;
import epicode.be.ees.entities.Invoice;

/**
 * Repository per fatture.
 * 
 * @author Nello
 *
 */
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

	@Query("SELECT MAX(i.number) + 1 FROM invoices i WHERE i.year = :year")
	Optional<Integer> findNextNumberOfYear(Integer year);

	Optional<Invoice> findByNumberAndYear(Integer number, Integer year);

	Page<Invoice> findAllByCustomerId(Long customerId, Pageable pageable);

	/**
	 * Fatturato annuo per cliente.
	 * 
	 * @param custId l'id del cliente.
	 * @param year   l'anno di riferimento.
	 */
	@Query("SELECT SUM(i.amount) FROM invoices i WHERE i.customer.id=:custId AND i.year=:year")
	Optional<BigDecimal> getAmountByCustomerAndYear(long custId, int year);

	/**
	 * Informazioni sul fatturato.
	 * 
	 * @author Nello
	 *
	 */
	public interface AmountByYear {
		/**
		 * @return il cliente
		 */
		Customer getCustomer();

		/**
		 * @return il fatturato
		 */
		BigDecimal getAmount();
	}

	/**
	 * Fatturato annuo per tutti i clienti.
	 * 
	 * @param year anno di riferimento.
	 */
	@Query(value = "SELECT (SELECT c FROM customers c WHERE c.id = i.customer.id) as customer, "
			+ "SUM(i.amount) as amount FROM invoices i WHERE i.year=:year GROUP BY i.customer.id")
	List<AmountByYear> getAmountByYear(int year);

	/**
	 * Fatturato annuo organizzato per clienti e paginato.
	 * 
	 * @param year     anno di riferimento.
	 * @param pageable regole di paginazione e ordinamento.
	 */
	@Query(value = "SELECT (SELECT c FROM customers c WHERE c.id = i.customer.id) as customer, "
			+ "SUM(i.amount) as amount FROM invoices i WHERE i.year=:year GROUP BY i.customer.id", // 
			countQuery = "SELECT COUNT(*) FROM invoices i WHERE i.year=:year GROUP BY i.customer.id")
	Page<AmountByYear> getPagedAmountByYear(int year, Pageable pageable);

	@Query(value = "SELECT (SELECT c FROM customers c WHERE c.id = i.customer.id) as customer, SUM(i.amount) as amount FROM invoices i WHERE i.year=:year GROUP BY i.customer.id HAVING SUM(i.amount) >= :min AND SUM(i.amount) <= :max", countQuery = "SELECT COUNT(*) FROM invoices i WHERE i.year=:year GROUP BY i.customer.id HAVING SUM(i.amount) >= :min AND SUM(i.amount) <= :max")
	Page<AmountByYear> getPagedAmountByYear(int year, BigDecimal min, BigDecimal max, Pageable pageable);

	Page<Invoice> getByCustomerId(long customerId, Pageable pageable);

	Page<Invoice> findAllByCustomerIdAndYear(long customerId, Integer year, Pageable pageable);

	Page<Invoice> findAllByYear(Integer year, Pageable pageable);

}
