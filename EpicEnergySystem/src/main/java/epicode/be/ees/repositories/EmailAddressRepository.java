package epicode.be.ees.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import epicode.be.ees.entities.addresses.EmailAddress;

/**
 * Repository per emails.
 * 
 * @author Nello.
 *
 */
@Repository
public interface EmailAddressRepository extends JpaRepository<EmailAddress, Long> {
	/**
	 * Ricerca per email.
	 * 
	 * @param email l'indirizzo email.
	 */
	Optional<EmailAddress> findByEmail(String email);
}
