package epicode.be.ees.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import epicode.be.ees.entities.InvoiceStatus;

/**
 * Repository per stato fattura.
 * 
 * @author Nello
 *
 */
@Repository
public interface InvoiceStatusRepository extends JpaRepository<InvoiceStatus, Long> {

	List<InvoiceStatus> findAllByNameContains(String status);

	Optional<InvoiceStatus> findByName(String status);

}
