package epicode.be.ees.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import epicode.be.ees.entities.Customer;

/**
 * Repository per clienti.
 * 
 * @author Nello
 *
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	Page<Customer> findAllByNameContainsIgnoreCase(String searchFor, Pageable pageable);

	Page<Customer> findAllByLastContactBetween(Date from, Date to, Pageable pageable);

	Page<Customer> findAllByCreatedAtBetween(Date from, Date to, Pageable pageable);

	Page<Customer> findAllByTypeTypeIgnoreCase(String type, Pageable pageable);

	Page<Customer> findAllByNameContainsIgnoreCaseAndTypeTypeIgnoreCase(String searchFor, String type, Pageable pageable);

}
