package epicode.be.ees.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import epicode.be.ees.entities.addresses.Address;

/**
 * Repository per indirizzi.
 * 
 * @author Nello
 *
 */
public interface AddressRepository extends JpaRepository<Address, Long> {

}
