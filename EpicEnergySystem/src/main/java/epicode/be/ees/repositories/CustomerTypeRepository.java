package epicode.be.ees.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import epicode.be.ees.entities.CustomerType;

/**
 * Repository per tipologia di cliente.
 * 
 * @author Nello
 *
 */
@Repository
public interface CustomerTypeRepository extends JpaRepository<CustomerType, Long> {

	List<CustomerType> findAllByTypeContains(String type);

	Optional<CustomerType> findByType(String type);

}
