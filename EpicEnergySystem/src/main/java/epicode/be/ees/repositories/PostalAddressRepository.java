package epicode.be.ees.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import epicode.be.ees.entities.addresses.PostalAddress;

/**
 * Un repository per indirizzi postali.
 * 
 * @author Nello
 *
 */
@Repository
public interface PostalAddressRepository extends JpaRepository<PostalAddress, Long> {

}
