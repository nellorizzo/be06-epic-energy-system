package epicode.be.ees.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import epicode.be.ees.entities.addresses.City;

/**
 * Repository per città.
 * 
 * @author Nello
 *
 */
@Repository
public interface CityRepository extends JpaRepository<City, Long> {
	/**
	 * Recupera le città di una provincia.
	 * 
	 * @param provinceAcronym la sigla della provincia.
	 */
	Page<City> findAllByProvinceAcronym(String provinceAcronym, Pageable pageable);

	/**
	 * Recupera le città per nome e per provincia.
	 * 
	 * @param name            parte del nome della città.
	 * @param provinceAcronym la provincia.
	 */
	Page<City> findAllByNameContainsAndProvinceAcronym(String name, String provinceAcronym, Pageable pageable);

	/**
	 * Recupera una città.
	 * 
	 * @param name            il nome della città.
	 * @param provinceAcronym la sigla della provincia.
	 * @return
	 */
	Optional<City> findByNameAndProvinceAcronym(String name, String provinceAcronym);
}
