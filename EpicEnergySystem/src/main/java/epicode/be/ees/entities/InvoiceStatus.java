package epicode.be.ees.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Stato fattura.
 * 
 * @author Nello
 *
 */
@Entity(name = "invoice_status")
@SequenceGenerator(name = BaseEntity.SEQUENCE_GENERATOR, sequenceName = "invoicestatus_seq")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceStatus extends BaseEntity {
	@Column(unique = true, nullable = false, length = 15)
	private String name;
	@Column(nullable = true, length = 120)
	private String description;

	@Builder(setterPrefix = "with")
	private InvoiceStatus(Long id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
}
