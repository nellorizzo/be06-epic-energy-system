package epicode.be.ees.entities.addresses;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Posizione geografica di una città.
 * 
 * @author Nello
 *
 */
@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class GeoLocation {
	/**
	 * Longitudine.
	 */
	@Column(nullable = true)
	private float longitude;
	/**
	 * Latitudine.
	 */
	@Column(nullable = true)
	private float latitude;
}
