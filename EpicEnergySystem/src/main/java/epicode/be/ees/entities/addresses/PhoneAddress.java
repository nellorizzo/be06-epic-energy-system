package epicode.be.ees.entities.addresses;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Un numero di telefono.
 * 
 * @author Nello
 *
 */
@Entity(name = "phones")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class PhoneAddress extends Address {
	/**
	 * Il numero di telefono.
	 */
	@EqualsAndHashCode.Include
	@Column(nullable = false, unique = true, length = 20)
	private String number;

	@Builder(setterPrefix = "with")
	private PhoneAddress(Long id, String phoneNumber) {
		this.id = id;
		this.number = phoneNumber;
	}
}
