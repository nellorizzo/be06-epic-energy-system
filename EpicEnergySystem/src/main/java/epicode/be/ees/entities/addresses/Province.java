package epicode.be.ees.entities.addresses;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import epicode.be.ees.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Una provincia.
 * 
 * @author Nello
 *
 */
@Entity(name = "provinces")
@SequenceGenerator(name = BaseEntity.SEQUENCE_GENERATOR, sequenceName = "provinces_seq")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class Province extends BaseEntity {
	/**
	 * Denominazione.
	 */
	@Column(length = 50, nullable = false, unique = true)
	private String name;
	/**
	 * Sigla.
	 */
	@EqualsAndHashCode.Include
	@Column(length = 3, nullable = false, unique = true)
	private String acronym;
	/**
	 * Elenco delle città.
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "province")
	private final Set<City> cities = new HashSet<>();

	@Builder(setterPrefix = "with")
	private Province(Long id, String name, String acronym) {
		this.id = id;
		this.name = name;
		this.acronym = acronym;
	}
}
