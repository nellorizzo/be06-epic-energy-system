package epicode.be.ees.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import epicode.be.ees.entities.addresses.EmailAddress;
import epicode.be.ees.entities.addresses.PhoneAddress;
import epicode.be.ees.entities.addresses.PostalAddress;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Cliente.
 * 
 * @author Nello
 *
 */
@Entity(name = "customers")
@SequenceGenerator(name = BaseEntity.SEQUENCE_GENERATOR, sequenceName = "customers_seq")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(indexes = { @Index(columnList = "pec_id", unique = true, name = "unique_pec"),
		@Index(columnList = "email_id", unique = true, name = "unique_email"),
		@Index(columnList = "name", unique = true, name = "unique_name"),
		@Index(columnList = "vat", unique = true, name = "unique_vat"), })
public class Customer extends BaseEntity {
	/**
	 * Denominazione.
	 */
	@Column(nullable = false, length = 80)
	private String name;
	/**
	 * Partita IVA.
	 */
	@Column(nullable = false, length = 20)
	private String vat;
	/**
	 * Email.
	 */
	@OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private EmailAddress email;
	/**
	 * Data di ultimo contatto.
	 */
	private Date lastContact;
	/**
	 * Pec.
	 */
	@OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private EmailAddress pec;
	/**
	 * Telefono.
	 */
	@OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private PhoneAddress phone;
	/**
	 * Dati del contatto all'interno dell'azienda.
	 */
	@Embedded
	private Contact contact;
	/**
	 * Indirizzo legale.
	 */
	@OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private PostalAddress businessAddress;
	/**
	 * Indirizzo operativo.
	 */
	@OneToOne(optional = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private PostalAddress workAddress;
	/**
	 * Tipo di cliente.
	 */
	@ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.DETACH)
	private CustomerType type;

	@Builder(setterPrefix = "with")
	private Customer(Long id, String name, String vat, EmailAddress email, Date lastContact, EmailAddress pec,
			PhoneAddress phone, Contact contact, PostalAddress businessAddress, PostalAddress workAddress,
			CustomerType type) {
		this.id = id;
		this.name = name;
		this.vat = vat;
		this.email = email;
		this.lastContact = lastContact;
		this.pec = pec;
		this.phone = phone;
		this.contact = contact;
		this.businessAddress = businessAddress;
		this.workAddress = workAddress;
		this.type = type;
	}
}
