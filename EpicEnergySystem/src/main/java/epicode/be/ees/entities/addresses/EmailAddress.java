package epicode.be.ees.entities.addresses;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Posta elettronica.
 * 
 * @author Nello
 *
 */
@Entity(name = "emails")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class EmailAddress extends Address {
	/**
	 * L'email.
	 */
	@EqualsAndHashCode.Include
	@Column(nullable = false, length = 120, unique = true)
	private String email;

	@Builder(setterPrefix = "with")
	private EmailAddress(Long id, String email) {
		this.id = id;
		this.email = email;
	}
}
