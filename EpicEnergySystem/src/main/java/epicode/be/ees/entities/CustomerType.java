package epicode.be.ees.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Tipologia di cliente.
 * @author Nello
 *
 */
@Entity(name = "types")
@SequenceGenerator(name = BaseEntity.SEQUENCE_GENERATOR, sequenceName = "types_seq")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class CustomerType extends BaseEntity {
	/**
	 * Nome.
	 */
	@Column(nullable = false, length = 10)
	private String type;
	/**
	 * Descrizione.
	 */
	@Column(nullable = true, length = 50)
	private String description;

	@Builder(setterPrefix = "with")
	private CustomerType(Long id, String type, String description) {
		this.id = id;
		this.type = type;
		this.description = description;
	}
}
