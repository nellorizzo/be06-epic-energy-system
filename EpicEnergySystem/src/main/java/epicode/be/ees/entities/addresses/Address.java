package epicode.be.ees.entities.addresses;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;

import epicode.be.ees.entities.BaseEntity;

/**
 * Base per gli indirizzi.
 * 
 * @author Nello
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(name = BaseEntity.SEQUENCE_GENERATOR, sequenceName = "addresses_seq")
public abstract class Address extends BaseEntity {

}
