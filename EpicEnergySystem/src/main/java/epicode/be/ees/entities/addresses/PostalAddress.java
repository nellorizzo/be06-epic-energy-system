package epicode.be.ees.entities.addresses;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Un indirizzo postale.
 * 
 * @author Nello
 *
 */
@Entity(name = "postaladdresses")
@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class PostalAddress extends Address {
	/**
	 * Via.
	 */
	@Column(length = 125, nullable = false)
	private String street;
	/**
	 * Numero civico.
	 */
	@Column(length = 15, nullable = true)
	private String civicNumber;
	/**
	 * Località.
	 */
	@Column(length = 20, nullable = true)
	private String place;
	@Column(length = 5, nullable = false)
	/**
	 * C.a.p.
	 */
	private String zip;
	/**
	 * Città.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	private City city;

	@Builder(setterPrefix = "with")
	private PostalAddress(Long id, String street, String civicNumber, String place, String zip, City city) {
		this.id = id;
		this.street = street;
		this.civicNumber = civicNumber;
		this.place = place;
		this.city = city;
		this.zip = zip;
	}
}
