package epicode.be.ees.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Fattura.
 * 
 * @author Nello
 *
 */
@Entity(name = "invoices")
@SequenceGenerator(name = BaseEntity.SEQUENCE_GENERATOR, sequenceName = "invoices_seq")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(indexes = { @Index(columnList = "year, number", unique = true, name = "invoice_unique") })
public class Invoice extends BaseEntity {
	/**
	 * Data di pubblicazione.
	 */
	@Column(nullable = false)
	private Date publishDate;
	/**
	 * Anno della fattura.
	 */
	@Column(nullable = false)
	private Integer year;
	/**
	 * Importo.
	 */
	@Column(nullable = false)
	private BigDecimal amount;
	/**
	 * Numero della fattura.
	 */
	@Column(nullable = false)
	private Integer number;
	/**
	 * Stato della fattura.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	private InvoiceStatus status;
	/**
	 * Cliente.
	 */
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
	private Customer customer;

	@Builder(setterPrefix = "with")
	private Invoice(Long id, Date publishDate, Integer year, BigDecimal amount, Integer number, InvoiceStatus status,
			Customer customer) {
		this.id = id;
		this.publishDate = publishDate;
		this.amount = amount;
		this.number = number;
		this.year = year;
		this.status = status;
		this.customer = customer;
	}
}
