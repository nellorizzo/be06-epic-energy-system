package epicode.be.ees.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import epicode.be.ees.entities.addresses.EmailAddress;
import epicode.be.ees.entities.addresses.PhoneAddress;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Contatto di un'azienda.
 * 
 * @author Nello
 *
 */
@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with")
public class Contact {
	/**
	 * Nome.
	 */
	@Column(length = 80, name = "contact_name")
	private String name;
	/**
	 * Cognome.
	 */
	@Column(length = 80, name = "contact_surname")
	private String surname;
	/**
	 * Email.
	 */
	@OneToOne(optional = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "contact_email_id", nullable = true)
	private EmailAddress email;
	/**
	 * Numero di telefono.
	 */
	@OneToOne(optional = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "contact_phone_id", nullable = true)
	private PhoneAddress phone;
}
