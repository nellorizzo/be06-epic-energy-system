package epicode.be.ees.entities.addresses;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import epicode.be.ees.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Città.
 * 
 * @author Nello
 *
 */
@Entity(name = "cities")
@SequenceGenerator(name = BaseEntity.SEQUENCE_GENERATOR, sequenceName = "cities_seq")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class City extends BaseEntity {
	/**
	 * Denominazione.
	 */
	@EqualsAndHashCode.Include
	@Column(nullable = false, length = 80)
	private String name;
	/**
	 * Provincia.
	 */
	@EqualsAndHashCode.Include
	@ManyToOne(fetch = FetchType.LAZY)
	private Province province;
	/**
	 * Codice per il codice fiscale.
	 */
	@Column(nullable = true, length = 4)
	private String fiscalCode;
	/**
	 * Indica se è capoluogo di provincia.
	 */
	private boolean capital;
	@Embedded
	private GeoLocation location;

	@Builder(setterPrefix = "with")
	private City(Long id, String name, Province province, String fiscalCode, boolean capital, GeoLocation location) {
		this.id = id;
		this.name = name;
		this.province = province;
		this.fiscalCode = fiscalCode;
		this.capital = capital;
		this.location = location;
	}
}
