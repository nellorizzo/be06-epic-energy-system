package epicode.be.ees.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entità di base.
 * 
 * @author Nello
 *
 */
@MappedSuperclass
@Data
@NoArgsConstructor
public class BaseEntity {
	/**
	 * Nome per la condivisione della sequenza tra tutte le sottoclassi. Deve essere
	 * impostato nei
	 * {@code SequenceGenerator(generator = BaseEntity.SEQUENCE_GENERATOR} delle
	 * sottoclassi per avere una sequenza che sia in grado di popolare i valori per
	 * le chiavi delle sottoclassi.
	 */
	public static final String SEQUENCE_GENERATOR = "sequence_generator";
	/**
	 * Chiave.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_GENERATOR)
	protected Long id;
	/**
	 * Data di creazione.
	 */
	@Column(insertable = false, updatable = false, columnDefinition = "timestamp default current_timestamp")
	private Date createdAt;
}
