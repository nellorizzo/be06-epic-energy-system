package epicode.be.ees.dto.converters;

import epicode.be.ees.entities.BaseEntity;

/**
 * Interfaccia di conversione da Entity verso DTO
 * 
 * @author Nello
 *
 * @param <D> Tipo di DTO
 * @param <E> Tipo di Entity
 */
public interface DtoFromEntityConverter<D, E extends BaseEntity> {
	/**
	 * Costruisce un DTO a partire da un'entity.
	 * @param entity l'entity
	 */
	D convert(E entity);
}
