package epicode.be.ees.dto.converters;

import epicode.be.ees.dto.CustomerDto;
import epicode.be.ees.entities.Customer;
/**
 * Converte un cliente da Entity a DTO.
 * 
 * @author Nello
 *
 */
public interface CustomerFromEntityConverter extends DtoFromEntityConverter<CustomerDto, Customer>{

}
