package epicode.be.ees.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Dto per provincia.
 * 
 * @author Nello
 *
 */
@Data
@AllArgsConstructor
@Builder(setterPrefix = "with")
@ToString
public class ProvinceDto {
	private Long id;
	private Date createdAt;
	private String name;
	private String acronym;
}
