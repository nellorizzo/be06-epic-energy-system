package epicode.be.ees.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto per indirizzo postale.
 * 
 * @author Nello
 *
 */
@Data
@Builder(setterPrefix = "with")
@AllArgsConstructor
@NoArgsConstructor
public class PostalAddressDto {
	private Long id;
	private String street;
	private String civic;
	private String zip;
	private String location;
	private CityDto city;
}
