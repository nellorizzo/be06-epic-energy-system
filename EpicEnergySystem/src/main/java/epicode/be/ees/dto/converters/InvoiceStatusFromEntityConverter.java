package epicode.be.ees.dto.converters;

import epicode.be.ees.dto.InvoiceStatusDto;
import epicode.be.ees.entities.InvoiceStatus;

/**
 * Convertitore per stato fatture da Entity a DTO.
 * 
 * @author Nello
 *
 */
public interface InvoiceStatusFromEntityConverter extends DtoFromEntityConverter<InvoiceStatusDto, InvoiceStatus> {

}
