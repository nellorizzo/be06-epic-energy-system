package epicode.be.ees.dto.converters;

import epicode.be.ees.dto.CustomerTypeDto;
import epicode.be.ees.entities.CustomerType;

/**
 * Converte un tipo cliente da Entity a DTO.
 * 
 * @author Nello
 *
 */
public interface CustomerTypeFromEntityConverter extends DtoFromEntityConverter<CustomerTypeDto, CustomerType> {

}
