package epicode.be.ees.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder(setterPrefix = "with")
@AllArgsConstructor
@ToString
public class InvoiceStatusDto {
	private Long id;
	private String name;
	private String description;
}
