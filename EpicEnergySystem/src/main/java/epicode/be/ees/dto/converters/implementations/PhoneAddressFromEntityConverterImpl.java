package epicode.be.ees.dto.converters.implementations;

import org.springframework.stereotype.Component;

import epicode.be.ees.dto.converters.PhoneAddressFromEntityConverter;
import epicode.be.ees.entities.addresses.PhoneAddress;

@Component
public class PhoneAddressFromEntityConverterImpl implements PhoneAddressFromEntityConverter {

	@Override
	public String convert(PhoneAddress entity) {
		return entity == null ? null : entity.getNumber();
	}

}
