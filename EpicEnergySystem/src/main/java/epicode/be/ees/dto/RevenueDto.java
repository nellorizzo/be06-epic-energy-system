package epicode.be.ees.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class RevenueDto {
	private long customerId;
	private String customerName;
	private String customerVat;
	private BigDecimal revenues;
}
