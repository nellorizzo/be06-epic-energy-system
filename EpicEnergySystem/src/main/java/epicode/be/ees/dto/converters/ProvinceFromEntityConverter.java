package epicode.be.ees.dto.converters;

import epicode.be.ees.dto.ProvinceDto;
import epicode.be.ees.entities.addresses.Province;

/**
 * Converte una provincia da Entity a DTO.
 * 
 * @author Nello
 *
 */
public interface ProvinceFromEntityConverter extends DtoFromEntityConverter<ProvinceDto, Province> {

}
