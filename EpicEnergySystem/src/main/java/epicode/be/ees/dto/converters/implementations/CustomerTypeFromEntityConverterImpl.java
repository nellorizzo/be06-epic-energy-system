package epicode.be.ees.dto.converters.implementations;

import org.springframework.stereotype.Component;

import epicode.be.ees.dto.CustomerTypeDto;
import epicode.be.ees.dto.converters.CustomerTypeFromEntityConverter;
import epicode.be.ees.entities.CustomerType;

@Component
public class CustomerTypeFromEntityConverterImpl implements CustomerTypeFromEntityConverter {

	@Override
	public CustomerTypeDto convert(CustomerType entity) {
		if (entity == null)
			return null;
		return CustomerTypeDto.builder().withCreatedAt(entity.getCreatedAt()).withDescription(entity.getDescription())
				.withId(entity.getId()).withType(entity.getType()).build();
	}

}
