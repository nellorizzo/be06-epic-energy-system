package epicode.be.ees.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder(setterPrefix = "with")
@AllArgsConstructor
@ToString
public class InvoiceDto {
	private Long id;
	private Date createdAt;
	private Date publishDate;
	private Integer year;
	private BigDecimal amount;
	private Integer number;
	private long customerId;
	private String status;
}
