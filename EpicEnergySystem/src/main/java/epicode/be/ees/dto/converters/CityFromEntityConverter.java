package epicode.be.ees.dto.converters;

import epicode.be.ees.dto.CityDto;
import epicode.be.ees.entities.addresses.City;

/**
 * Converte una città da Entity a DTO.
 * 
 * @author Nello
 *
 */
public interface CityFromEntityConverter extends DtoFromEntityConverter<CityDto, City> {


}
