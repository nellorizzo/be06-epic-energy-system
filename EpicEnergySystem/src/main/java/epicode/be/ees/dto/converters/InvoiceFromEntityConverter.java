package epicode.be.ees.dto.converters;

import epicode.be.ees.dto.InvoiceDto;
import epicode.be.ees.entities.Invoice;

/**
 * Convertitore per fatture da Entity a DTO.
 * 
 * @author Nello
 *
 */
public interface InvoiceFromEntityConverter extends DtoFromEntityConverter<InvoiceDto, Invoice> {

}
