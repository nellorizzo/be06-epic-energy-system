package epicode.be.ees.dto.converters;

import epicode.be.ees.entities.addresses.EmailAddress;

/**
 * Converte una email da Entity a DTO.
 * 
 * @author Nello
 *
 */
public interface EmailAddressFromEntityConverter extends DtoFromEntityConverter<String, EmailAddress>{

}
