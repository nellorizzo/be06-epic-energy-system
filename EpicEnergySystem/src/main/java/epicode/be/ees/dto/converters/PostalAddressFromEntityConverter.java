package epicode.be.ees.dto.converters;

import epicode.be.ees.dto.PostalAddressDto;
import epicode.be.ees.entities.addresses.PostalAddress;

/**
 * Converte un indirizzo postale da Entity a DTO.
 * 
 * @author Nello
 *
 */
public interface PostalAddressFromEntityConverter extends DtoFromEntityConverter<PostalAddressDto, PostalAddress> {

}
