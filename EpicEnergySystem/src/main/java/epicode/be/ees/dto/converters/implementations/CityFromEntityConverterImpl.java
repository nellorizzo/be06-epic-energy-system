package epicode.be.ees.dto.converters.implementations;

import org.springframework.stereotype.Component;

import epicode.be.ees.dto.CityDto;
import epicode.be.ees.dto.converters.CityFromEntityConverter;
import epicode.be.ees.entities.addresses.City;

@Component
public class CityFromEntityConverterImpl implements CityFromEntityConverter {

	@Override
	public CityDto convert(City entity) {
		if (entity == null)
			return null;
		var province = entity.getProvince() == null ? null : entity.getProvince().getAcronym();
		return CityDto.builder().withCapital(entity.isCapital()).withCreatedAt(entity.getCreatedAt())
				.withFiscalCode(entity.getFiscalCode()).withId(entity.getId()).withName(entity.getName())
				.withProvinceAcronym(province)
				.withLatitude(entity.getLocation() == null ? null : entity.getLocation().getLatitude())
				.withLongitude(entity.getLocation() == null ? null : entity.getLocation().getLatitude()).build();
	}
}
