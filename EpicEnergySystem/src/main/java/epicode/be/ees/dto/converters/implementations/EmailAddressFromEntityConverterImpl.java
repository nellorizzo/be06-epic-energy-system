package epicode.be.ees.dto.converters.implementations;

import org.springframework.stereotype.Component;

import epicode.be.ees.dto.converters.EmailAddressFromEntityConverter;
import epicode.be.ees.entities.addresses.EmailAddress;

@Component
public class EmailAddressFromEntityConverterImpl implements EmailAddressFromEntityConverter {

	@Override
	public String convert(EmailAddress entity) {
		return entity == null ? null : entity.getEmail();
	}

}
