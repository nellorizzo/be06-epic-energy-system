package epicode.be.ees.dto;

import java.util.Date;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto per cliente.
 * 
 * @author Nello
 *
 */
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class CustomerDto {
	private Long id;
	private Date createdAt;
	private String name;
	private String vat;
	private String email;
	private String phone;
	private Date lastContact;
	private String pec;
	private PostalAddressDto businessAddress;
	private PostalAddressDto workAddress;
	private ContactDto contact;
	private String type;
}
