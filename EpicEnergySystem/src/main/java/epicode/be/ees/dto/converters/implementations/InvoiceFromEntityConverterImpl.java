package epicode.be.ees.dto.converters.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import epicode.be.ees.dto.InvoiceDto;
import epicode.be.ees.dto.converters.CustomerFromEntityConverter;
import epicode.be.ees.dto.converters.InvoiceFromEntityConverter;
import epicode.be.ees.entities.Invoice;

@Component
public class InvoiceFromEntityConverterImpl implements InvoiceFromEntityConverter {

	@Autowired
	CustomerFromEntityConverter custConv;

	@Override
	public InvoiceDto convert(Invoice entity) {
		return InvoiceDto.builder().withAmount(entity.getAmount()).withCreatedAt(entity.getCreatedAt())
				.withCustomerId(entity.getCustomer().getId()).withId(entity.getId())
				.withNumber(entity.getNumber()).withPublishDate(entity.getPublishDate())
				.withStatus(entity.getStatus().getName()).withYear(entity.getYear()).build();
	}

}
