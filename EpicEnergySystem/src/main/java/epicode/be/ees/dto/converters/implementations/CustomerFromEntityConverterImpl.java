package epicode.be.ees.dto.converters.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import epicode.be.ees.dto.ContactDto;
import epicode.be.ees.dto.CustomerDto;
import epicode.be.ees.dto.converters.CustomerFromEntityConverter;
import epicode.be.ees.dto.converters.CustomerTypeFromEntityConverter;
import epicode.be.ees.dto.converters.EmailAddressFromEntityConverter;
import epicode.be.ees.dto.converters.PhoneAddressFromEntityConverter;
import epicode.be.ees.dto.converters.PostalAddressFromEntityConverter;
import epicode.be.ees.entities.Contact;
import epicode.be.ees.entities.Customer;

@Component
public class CustomerFromEntityConverterImpl implements CustomerFromEntityConverter {

	@Autowired
	PostalAddressFromEntityConverter postalAddressConverter;
	@Autowired
	EmailAddressFromEntityConverter emailConverter;
	@Autowired
	PhoneAddressFromEntityConverter phoneConverter;
	@Autowired
	CustomerTypeFromEntityConverter customerTypeConverter;

	@Override
	public CustomerDto convert(Customer entity) {
		if (entity == null)
			return null;
		return CustomerDto.builder().withBusinessAddress(postalAddressConverter.convert(entity.getBusinessAddress()))
				.withContact(convertContact(entity.getContact())).withCreatedAt(entity.getCreatedAt())
				.withEmail(emailConverter.convert(entity.getEmail())).withId(entity.getId())
				.withLastContact(entity.getLastContact()).withName(entity.getName())
				.withPec(emailConverter.convert(entity.getPec())).withPhone(phoneConverter.convert(entity.getPhone())).withVat(entity.getVat())
				.withWorkAddress(postalAddressConverter.convert(entity.getWorkAddress()))
				.withType(customerTypeConverter.convert(entity.getType()).getType())
				.build();
	}

	private ContactDto convertContact(Contact contact) {
		if (contact == null)
			return null;
		return ContactDto.builder().withEmail(emailConverter.convert(contact.getEmail())).withName(contact.getName())
				.withPhone(phoneConverter.convert(contact.getPhone())).withSurname(contact.getSurname()).build();
	}

}
