package epicode.be.ees.dto.converters;

import epicode.be.ees.entities.addresses.PhoneAddress;

/**
 * Converte un numero di telefono da Entity a DTO.
 * 
 * @author Nello
 *
 */
public interface PhoneAddressFromEntityConverter extends DtoFromEntityConverter<String, PhoneAddress>{

}
