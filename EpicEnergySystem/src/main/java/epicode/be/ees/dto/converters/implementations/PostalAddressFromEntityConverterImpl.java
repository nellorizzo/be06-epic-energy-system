package epicode.be.ees.dto.converters.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import epicode.be.ees.dto.PostalAddressDto;
import epicode.be.ees.dto.converters.CityFromEntityConverter;
import epicode.be.ees.dto.converters.PostalAddressFromEntityConverter;
import epicode.be.ees.entities.addresses.PostalAddress;

@Component
public class PostalAddressFromEntityConverterImpl implements PostalAddressFromEntityConverter {

	@Autowired
	CityFromEntityConverter cityConverter;

	@Override
	public PostalAddressDto convert(PostalAddress entity) {
		if (entity == null)
			return null;
		return PostalAddressDto.builder().withCivic(entity.getCivicNumber()).withId(entity.getId())
				.withCity(cityConverter.convert(entity.getCity())).withLocation(entity.getPlace())
				.withStreet(entity.getStreet()).withZip(entity.getZip()).build();
	}

}
