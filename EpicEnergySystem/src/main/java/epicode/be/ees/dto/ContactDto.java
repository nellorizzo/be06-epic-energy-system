package epicode.be.ees.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto per contatto interno ad un cliente.
 * 
 * @author Nello
 *
 */
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class ContactDto {
	private String name;
	private String surname;
	private String phone;
	private String email;
}
