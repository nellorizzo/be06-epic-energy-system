package epicode.be.ees.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Dto per tipo di cliente.
 * 
 * @author Nello
 *
 */
@Data
@AllArgsConstructor
@Builder(setterPrefix = "with")
public class CustomerTypeDto {
	private Long id;
	private Date createdAt;
	private String type;
	private String description;
}
