package epicode.be.ees.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Dto per città.
 * 
 * @author Nello
 *
 */
@Data
@Builder(setterPrefix = "with")
@AllArgsConstructor
@ToString
public class CityDto {
	private Long id;
	private Date createdAt;
	private String name;
	private String provinceAcronym;
	private String fiscalCode;
	private boolean capital;
	private Float longitude;
	private Float latitude;
}
