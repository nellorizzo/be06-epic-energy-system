package epicode.be.ees.dto;

import java.util.LinkedList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Dto per dati paginati.
 * 
 * @author Nello
 *
 * @param <T> Tipo di dati inclusi in pagina.
 */
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class PagedResult<T> {
	private List<T> slice = new LinkedList<>();
	private Boolean isFirst;
	private Boolean isLast;
	private Integer pageSize;
	private Integer pageNumber;
	private Long totalItems;
	private Integer totalPages;
}
