package epicode.be.ees.dto.converters.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import epicode.be.ees.dto.CityDto;
import epicode.be.ees.dto.ContactDto;
import epicode.be.ees.dto.CustomerDto;
import epicode.be.ees.dto.PostalAddressDto;
import epicode.be.ees.entities.Contact;
import epicode.be.ees.entities.Customer;
import epicode.be.ees.entities.CustomerType;
import epicode.be.ees.entities.addresses.City;
import epicode.be.ees.entities.addresses.EmailAddress;
import epicode.be.ees.entities.addresses.PhoneAddress;
import epicode.be.ees.entities.addresses.PostalAddress;
import epicode.be.ees.repositories.CityRepository;
import epicode.be.ees.repositories.CustomerTypeRepository;
import epicode.be.ees.repositories.EmailAddressRepository;
import epicode.be.ees.repositories.PhoneNumbersRepository;
import epicode.be.ees.repositories.PostalAddressRepository;
import epicode.be.ees.repositories.ProvinceRepository;
import epicode.be.ees.services.exceptions.EntityNotFoundException;

@Component
public class EntitiesFromDtoConverters {

	@Autowired
	CityRepository cities;
	
	@Autowired
	ProvinceRepository provinces;

	@Autowired
	PostalAddressRepository postals;

	@Autowired
	EmailAddressRepository emails;

	@Autowired
	PhoneNumbersRepository phones;

	@Autowired
	CustomerTypeRepository types;

	public City fromDto(CityDto dto) {
		return cities.findByNameAndProvinceAcronym(dto.getName(), dto.getProvinceAcronym())
				.orElseThrow(() -> new EntityNotFoundException(
						String.format("{ city = %s, acronym = %s}", dto.getName(), dto.getProvinceAcronym()),
						CityDto.class));
	}

	public PostalAddress fromDto(PostalAddressDto dto) {
		if (dto == null)
			return null;
		if (dto.getId() != null && dto.getId() > 0)
			return postals.findById(dto.getId())
					.orElseThrow(() -> new EntityNotFoundException(dto.getId(), PostalAddressDto.class));
		return PostalAddress.builder().withCity(fromDto(dto.getCity())).withCivicNumber(dto.getCivic())
				.withId(dto.getId()).withPlace(dto.getLocation()).withStreet(dto.getStreet()).withZip(dto.getZip())
				.build();
	}

	public EmailAddress emailFromDto(String text) {
		return emails.findByEmail(text).orElse(EmailAddress.builder().withEmail(text).build());
	}

	public PhoneAddress phoneFromDto(String text) {
		return phones.findByNumber(text).orElse(PhoneAddress.builder().withPhoneNumber(text).build());
	}

	public CustomerType typeFromDto(String text) {
		return types.findByType(text).orElse(CustomerType.builder().withType(text).build());
	}

	public Contact fromDto(ContactDto dto) {
		if (dto == null)
			return null;
		var email = dto.getEmail() != null ? emailFromDto(dto.getEmail()) : null;
		var phone = dto.getPhone() != null ? phoneFromDto(dto.getPhone()) : null;
		return Contact.builder().withEmail(email).withName(dto.getName()).withPhone(phone).withSurname(dto.getSurname())
				.build();
	}

	public Customer fromDto(CustomerDto dto) {
		if (dto == null)
			return null;
		return Customer.builder() //
				.withBusinessAddress(fromDto(dto.getBusinessAddress())) //
				.withContact(fromDto(dto.getContact())) //
				.withEmail(emailFromDto(dto.getEmail())) //
				.withId(dto.getId()) //
				.withLastContact(dto.getLastContact()) //
				.withName(dto.getName()) //
				.withPec(emailFromDto(dto.getPec())) //
				.withPhone(phoneFromDto(dto.getPhone())) //
				.withType(typeFromDto(dto.getType())) //
				.withVat(dto.getVat()) //
				.withWorkAddress(fromDto(dto.getWorkAddress())) //
				.build();
	}
}
