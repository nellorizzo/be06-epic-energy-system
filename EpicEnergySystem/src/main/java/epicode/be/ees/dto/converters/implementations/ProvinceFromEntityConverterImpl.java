package epicode.be.ees.dto.converters.implementations;

import org.springframework.stereotype.Component;

import epicode.be.ees.dto.ProvinceDto;
import epicode.be.ees.dto.converters.ProvinceFromEntityConverter;
import epicode.be.ees.entities.addresses.Province;

@Component
public class ProvinceFromEntityConverterImpl implements ProvinceFromEntityConverter {

	@Override
	public ProvinceDto convert(Province entity) {
		if (entity == null)
			return null;
		return ProvinceDto.builder().withAcronym(entity.getAcronym()).withCreatedAt(entity.getCreatedAt())
				.withId(entity.getId()).withName(entity.getName()).build();
	}

}
